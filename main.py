import random
import pickle

# Luodaan kolme dictionarya, joihin tallennetaan hahmonluonnissa
# tarvittavat vaihtoehdot.
sex = {"a": "Male", "b": "Female"}
race = {
    "a": "Human", 
    "b": "Elf", 
    "c": "Dwarf", 
    "d": "Halfling", 
    "e": "Half-Troll", 
    "f": "Gnome"
}
profession = {
    "a": "Warrior", 
    "b": "Mage", 
    "c": "Priest", 
    "d": "Rogue", 
    "e": "Paladin", 
}

# Luodaan lista hahmon tallentamista varten.
character_saved = []


# Luodaan luokka "Character", jonka avulla hahmo luodaan.
class Character:

    # Luokkaan kuuluvan olion constructori, joka määrittää ensin
    # mitä luokkaan "Character" kuuluvalle oliolle annetaan 
    # ominaisuuksiksi...
    def __init__(
        self, 
        name="", 
        sex="", 
        race="", 
        profession="", 
        strenght=0, 
        intelligence=0, 
        wisdom=0, 
        dexterity=0, 
        constitution=0,
        to_fight=0,    
        to_shoot=0,
        hit_points_roll=0,        
    ):
        # ... ja sitten miten nuo ominaisuudet luodaan.
        self.name = ask_for_name()
        self.sex = ask_for_sex()
        self.race = ask_for_race()
        self.profession = ask_for_class()        
        self.strenght = random.randint(3, 18)
        self.intelligence = random.randint(3, 18)
        self.wisdom = random.randint(3, 18)
        self.dexterity = random.randint(3, 18)
        self.constitution = random.randint(3, 18)
        self.to_fight = 0
        self.to_shoot = 0
        self.hit_points_roll = 0        
    
    # Lopuksi määritetään miten luokan tiedot muutetaan merkkijonoksi
    def __str__(self): return(
        "Name: " + self.name + "\n" + 
        "Sex: " + self.sex + "\n" + 
        "Race: " + self.race  + "\n" + 
        "Class: " + self.profession  + "\n" + 
        "Strenght: " + str(self.strenght)  + "\n" + 
        "Intelligence: " + str(self.intelligence)  + "\n" + 
        "Wisdom: " + str(self.wisdom)  + "\n" + 
        "Dexterity: " + str(self.dexterity)  + "\n" + 
        "Constitution: " + str(self.constitution) + "\n" +
        "Fighting: " + str(self.to_fight) + "\n" + 
        "Shooting: " + str(self.to_shoot) + "\n" + 
        "Hitpoints per level: " + str(self.hit_points_roll)
    )


# Seuraavaksi määritetään funktiot, joita edellä olevassa luokassa
# "Character" kutsutaan:

# Tehdään funktio, joka pyytää käyttäjää antamaan hahmolle nimen.
def ask_for_name():
    name = input("Please give character name: ")
    return name

# Tehdään funktio, joka pyytää käyttäjää valitsemaan hahmon
# sukupuolen.
def ask_for_sex():
    print("Choose 'a' for male, or")
    print("Choose 'b' for female")    
    
    try:
        while True:
            char_sex = input("Choose character sex: ")
            if char_sex == "a":
                char_sex = sex["a"]
                return char_sex
            elif char_sex == "b":
                char_sex = sex["b"]
                return char_sex
            else:
                print("")
                print("Wrong input!")
                print("Choose 'a' for male, or")
                print("Choose 'b' for female") 
    except:
        print("Something unexpected happened!")

# Tehdään funktio, joka pyytää käyttäjää valitsemaan hahmon rodun.
def ask_for_race():
    print("Choose 'a' for human, or")
    print("Choose 'b' for elf, or")
    print("Choose 'c' for dwarf, or")
    print("Choose 'd' for halfling, or")
    print("Choose 'e' for half-troll, or")
    print("Choose 'f' for gnome.")
    try:
        while True:
            char_race = input("Choose character race: ")
            if char_race == "a":
                char_race = race["a"]
                return char_race
            elif char_race == "b":
                char_race = race["b"]
                return char_race
            elif char_race == "c":
                char_race = race["c"]
                return char_race
            elif char_race == "d":
                char_race = race["d"]
                return char_race
            elif char_race == "e":
                char_race = race["e"]
                return char_race
            elif char_race == "f":
                char_race = race["f"]
                return char_race
            else:
                print("")
                print("Wrong input!")
                print("Choose 'a' for human, or")
                print("Choose 'b' for elf, or")
                print("Choose 'c' for dwarf, or")
                print("Choose 'd' for halfling, or")
                print("Choose 'e' for half-troll, or")
                print("Choose 'f' for gnome.")                
                
    except:
        print("Something unexpected happened!")

# Tehdään funktio, joka pyytää käyttäjää valitsemaan hahmon 
# hahmoluokan.
def ask_for_class():
    print("Choose 'a' for warrior, or")
    print("Choose 'b' for mage, or")
    print("Choose 'c' for priest, or")
    print("Choose 'd' for rogue, or")
    print("Choose 'e' for paladin.")
    try:
        while True:
            char_profession = input("Choose character class: ")
            if char_profession == "a":
                char_profession = profession["a"]
                return char_profession
            elif char_profession == "b":
                char_profession = profession["b"]
                return char_profession
            elif char_profession == "c":
                char_profession = profession["c"]
                return char_profession
            elif char_profession == "d":
                char_profession = profession["d"]
                return char_profession
            elif char_profession == "e":
                char_profession = profession["e"]
                return char_profession            
            else:
                print("")
                print("Wrong input!")
                print("Choose 'a' for warrior, or")
                print("Choose 'b' for mage, or")
                print("Choose 'c' for priest, or")
                print("Choose 'd' for rogue, or")
                print("Choose 'e' for paladin.")             
                 
    except:
        print("Something unexpected happened!")

# Määritetään funktio, joka muokkaa hahmon ominaisuuksia, 
# riippuen sukupuolesta.
def which_sex():
    if character.sex == "Male":
        character.strenght += 1
        character.dexterity -= 1

    elif character.sex == "Female":
        character.strenght -= 1
        character.dexterity += 1

# Määritetään funktio, joka muokkaa hahmon ominaisuuksia, 
# riippuen rodusta.
def which_race():
    if character.race == "Human":
        character.strenght += 0
        character.intelligence += 0
        character.wisdom += 0
        character.dexterity += 0
        character.constitution += 0
        character.to_fight += 0
        character.to_shoot += 0
        character.hit_points_roll += 10

    elif character.race == "Elf":
        character.strenght -= 1
        character.intelligence += 2
        character.wisdom -= 1
        character.dexterity += 1
        character.constitution -= 1
        character.to_fight -= 5
        character.to_shoot += 15
        character.hit_points_roll += 9

    elif character.race == "Dwarf":
        character.strenght -= 1
        character.intelligence += 2
        character.wisdom -= 1
        character.dexterity += 1
        character.constitution -= 1   
        character.to_fight += 15
        character.to_shoot += 0
        character.hit_points_roll += 11

    elif character.race == "Halfling":
        character.strenght -= 2
        character.intelligence += 2
        character.wisdom += 1
        character.dexterity += 3
        character.constitution += 2
        character.to_fight -= 10
        character.to_shoot += 20
        character.hit_points_roll += 7

    elif character.race == "Half-Troll":
        character.strenght += 4
        character.intelligence -= 4
        character.wisdom -= 2
        character.dexterity -= 4
        character.constitution += 3
        character.to_fight += 20
        character.to_shoot -= 10
        character.hit_points_roll += 12

    elif character.race == "Gnome":
        character.strenght -= 1
        character.intelligence += 2
        character.wisdom += 0
        character.dexterity += 2
        character.constitution += 1
        character.to_fight -= 8
        character.to_shoot += 12
        character.hit_points_roll += 8


# Määritetään funktio, joka muokkaa hahmon ominaisuuksia, 
# riippuen hahmoluokasta.
def which_profession():
    if character.profession == "Warrior":
        character.strenght += 3
        character.intelligence -= 2
        character.wisdom -= 2
        character.dexterity += 2
        character.constitution += 2
        character.to_fight += 70
        character.to_shoot += 55
        character.hit_points_roll += 9

    elif character.profession == "Mage":
        character.strenght -= 3
        character.intelligence += 3
        character.wisdom += 0
        character.dexterity += 0
        character.constitution -= 2
        character.to_fight += 35
        character.to_shoot += 20
        character.hit_points_roll += 0

    elif character.profession == "Priest":
        character.strenght -= 1
        character.intelligence -= 3
        character.wisdom += 3
        character.dexterity -= 1
        character.constitution += 1
        character.to_fight += 45
        character.to_shoot += 35
        character.hit_points_roll += 2

    elif character.profession == "Rogue":
        character.strenght += 0
        character.intelligence += 1
        character.wisdom -= 3
        character.dexterity += 3
        character.constitution -= 1
        character.to_fight += 35
        character.to_shoot += 66
        character.hit_points_roll += 4
        
    elif character.profession == "Paladin":
        character.strenght += 1
        character.intelligence -= 3
        character.wisdom += 1
        character.dexterity -= 1
        character.constitution += 2
        character.to_fight += 65
        character.to_shoot += 50
        character.hit_points_roll += 6


# Luodaan funktio, joka varmistaa, että hahmon ominaisuudet saavat
# minimissään arvon kolme.
def check_if_under_three():
    if character.strenght < 3:
        character.strenght = 3
        return character.strenght
    if character.intelligence < 3:
        character.intelligence = 3
        return character.intelligence
    if character.wisdom < 3:
        character.wisdom = 3
        return character.wisdom
    if character.dexterity < 3:
        character.dexterity = 3
        return character.dexterity
    if character.constitution < 3:
        character.constitution = 3
        return character.constitution


# Tehdään funktio hahmon tallentamista varten.
def save_character():

    # Määritetään muuttuja "filename", ja käytetään sitä
    # vastaisuudessa tiedoston nimen syöttämiseen.
    filename = "save.dat"

    # Lisätään aiemmin määritettyyn listaan "character_saved",
    # muutujan character tiedot.
    character_saved.append(character)
    
    # Tehdään hahmon tallennus "try-except"-rakenteen sisälle,
    # jotta ohjelma ei kaadu jos tallentaminen ei jostain syystä
    # onnistu.
    try:

        # Avataan tiedosto kirjoitustilaan (w) ja binääriseen
        # tilaan (b)
        file = open(filename, "wb")

        # Tyhjennetään tiedosto, ennen uuden tiedon tallentamista.
        file.truncate(0)

        # Käytetään "pickle"-kirjaston "dump()" funktiota, 
        # kirjoittamaan hahmo tiedostoon binäärisessä muodossa.
        pickle.dump(character_saved, file, pickle.HIGHEST_PROTOCOL)
    
    # Jos tiedoston avaaminen ei onnistu, tulostetaan virheilmoitus.
    except:
        print("Could not save to " + filename)

    # Lopuksi suljetaan tiedosto.
    finally:
        file.close()


# Tehdään loop, jossa pyydetään käyttäjää valitsemaan joko
# vanhan hahmon lataamisen tai uuden hahmon luomisen.
while True:
    load_or_roll = input("Would you like to (l)oad old character or create (n)ew character: ")

    # Jos käyttäjä valitsee 'n', luodaan uusi hahmo.
    if load_or_roll == "n":

        # Määritetään muuttuja "character", kutsumaan luokkaa "Character",
        # eli luodaan olio "character", joka kuuluu luokkaan "Character".
        # Olio "character" on käyttäjän hahmo.
        character = Character()

        # Kutsutaan funktioita, jotka muokkaavat hahmoa, käyttäjän tekemien
        # valintojen mukaan.
        which_sex()
        which_race()
        which_profession()

        # Varmistetaan että mikään arvo ei ole alle kolmen.
        check_if_under_three()

        # Tulostetaan saatu hahmo.
        print("")
        print("Your character is:")
        print(character)


        # Tehdään loop, joka kysyy haluaako käyttäjä tallentaa luodun hahmon,
        # vai tehdä uuden hahmon. Käyttäjän antaessa väärän syötteen,
        # palataan loopin alkuun kysymään haluaako käyttäjä tallentaa luodun 
        # hahmon, vai tehdä uuden hahmon.
        while True:
            choice = input("Choose 's' for save and exit or 'r' for re-roll: ")
            
            # Käyttäjän syöttäessä jotain muuta kuin 's' tai 'r'
            # tulostetaan: "Wrong input!" ja jatketaan loopissa.
            if choice != "s" and choice != "r":
                print("Wrong input!")
                continue
            
            # Käyttäjän syöttäessä 'r' aloitetaan hahmonluonti uudestaan
            # ja pysytään loopissa, jotta käyttäjä voi luoda hahmoja 
            # uudestaan kunnes on tyytyväinen.
            elif choice == "r":
                print("Again!")
                character = Character()
                which_sex()
                which_race()
                which_profession()
                check_if_under_three()
                print("")
                print("Your character is:")
                print(character)        
                continue

            # Käyttäjän syöttäessä 's' kutsutaan funktiota 
            # "save_character", eli tallennetaan hahmo ja 
            # poistutaan loopista.
            elif choice == "s":
                print("Saving...")
                print("Exiting...")
                save_character()
                break
        
        # Poistutaan loopista, jotta hahmon tallentamisen jälkeen,
        # ohjelman suorittaminen loppuu.
        break

    # Tässä palataan ensimmäiseen looppiin, eli aiemmin luodun hahmon
    # lataamiseen.
    elif load_or_roll == "l":
        filename = "save.dat"

        # Tehdään tiedoston käsittely "try-except"-rakenteen sisään,
        # jotta ohjelma ei kaadu, jos esim. vanhaa hahmoa ei ole 
        # olemassa.
        try:

            # Avataan tiedosto luku moodiin (r) ja binääriseen
            # moodiin (b).
            file = open(filename, "rb")

            # Lisätään listaan "character_saved" tiedoston
            # "file" sisältö, käyttämällä "pickle"-kirjaston
            # "load()"-funktiota.
            character_saved = pickle.load(file)

            # Suljetaan tiedosto.
            file.close()

            # Käytetään "for"-looppia iteroimaan listassa 
            # "character_saved" olevat elementit, joita kutsun
            # nimellä "character".
            for character in character_saved:
                # Tulostetaan edellä iteroimalla saadut tiedot.
                print(character)

            # Poistutaan loopista.
            break

        # Jos tiedoston lataamisessa tapahtuu virhe, tulostetaan
        # virheilmoitus...
        except:
            print("Unable to load character! Please choose 'n'!")
            # ... ja palataan loopin alkuun, jotta käyttäjä
            # voi luoda uuden hahmon.
            continue

    # Käyttäjän syöttäessä väärän syötteen, tulostetaan:
    # "Wrong input!" ja jäädään looppiin.
    elif load_or_roll != "n" and load_or_roll != "l":
        print("Wrong input!")
        continue