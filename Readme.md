# Tämä ohjelma on tarkoitettu roolipelin hahmonluomiseen.

Ensimmäisessä vaiheessa ohjelma pyytää käyttäjää valitsemaan:
* ladataanko jo luotu hahmo
* luodaanko uusi hahmo.

Jos valitaan lataaminen, lataa ohjelma hahmon tiedot tiedostosta
ja lopettaa ohjelman toiminnan.

Jos valitaan uusi hahmo ohjelma pyytää käyttäjää valitsemaan:
* hahmolle nimen
* hahmon sukupuolen
* hahmon rodun
* hahmon hahmoluokan.

Näiden valintojen jälkeen ohjelma arpoo, satunnaiset arvot
väliltä 3-18, hahmon seuraaville ominaisuuksille:
* strenght
* intelligence
* wisdom
* dexterity
* constitution.

Lisäksi ohjelma asettaa arvon 0 seuraaville ominaisuuksille:
* fighting
* shooting
* hitpoints per level

Tämän jälkeen ohjelma muokkaa edellä saatuja ominaisuuksia,
käyttäjän valintojen pohjalta.

Lopuksi ohjelma kysyy haluaako käyttäjä tallentaa hahmon
vai luoda hahmon uudelleen.

## Muuttujat:
* esimerkiksi riveillä 84, 95, 120...

## Ohjausrakenteet:
* "while"-loop esimerkiksi riveillä 94-106, 119-147, 377-488
* "for"-loop riveillä 469-471
* "if"-ehto esimerkiksi riveillä 322-336
* "if-elif"-ehto esimerkiksi riveillä 268-316
* "if-elif-else"-ehto esimerkiksi riveillä 161-185

## Vuorovaikutus:
* esimerkiksi 95-106, 378-488

## Kokoelmat:
* lista; rivillä 24, 348, 364, 461, 469
* dictionary; riveillä 6-21, 97, 100, 122, 125,...

## Funktiot:
* määrittäminen; 83-85, 89-108, 111-150, 154-188, 192-199, 
203-262, 267-316, 321-336 ja 340-372 
* kutsuminen; 390, 391, 392, 395,...
* parametrien käyttö; en tarvinnut omissa funktioissa,
mutta kirjastojen funktioita käyttäessä piti käyttää
parametrejä, esimerkiksi riveillä 364 ja 461.
* Olisin toki voinut tehdä sinne vaikka:
``` python
def laske_yhteen(numero1, numero2):
        summa = numero1 + numero2
        return summa
def jaa_keskenaan(numero3, numero4):
        jako = numero3 / numero4
        return jako

print(laske_yhteen(1, 5) * jaa_keskenaan(6, 54))
```
* ja saada vastaukseksi 0.666666666. Näin en kuitenkaan
tehnyt, koska en keksinyt oikeaa käyttöä ohjelmassani.

## Tietorakenteet:
* määrittäminen; 28-76
* olion luominen; 386
* olion ominaisuuksien kutsuminen ja muokkaaminen; esimerkiksi 268-316

## Poikkeusten käsittely:
* esimerkiksi riveillä: 353-372, 452-482

## Python kirjastojen käyttö:
* 1, 2, 53-57, 364, 461

## Tiedostojen käsittely:
* 340-372, 446-482